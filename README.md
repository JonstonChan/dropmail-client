dropmail-client
===

`dropmail-client` is a Python module containing an interface to https://dropmail.me, a free service that provides disposable email addresses.


Installation
---

`pip3 install dropmail-client`


Examples
---

Examples can be found in the [`examples` directory](https://gitlab.com/JonstonChan/dropmail-client/tree/stable/examples).