#!/usr/bin/env python3

import hashlib
import random
import string
import threading
import time
import websocket_server

supported_domains = ['dropmail.me', '10mail.org', 'yomail.info', 'emltmp.com', 'emlpro.com', 'emlhub.com']

def generate_id(length, chars=string.ascii_lowercase):
	return ''.join(random.choice(chars) for _ in range(length))

def generate_session_id():
	hex_characters = '0123456789abcdef'
	generate_hex_id = lambda length: generate_id(length, chars=hex_characters)

	id_lengths = [8, 4, 4, 4, 12]
	id = '-'.join([generate_hex_id(id_length) for id_length in id_lengths])

	return f'S{{"id":"{id}"}}'

def generate_account_info(domain, length=9):
	id = generate_id(length)
	email = id + '@' + domain

	key = '0' + hashlib.md5(email.encode('utf-8')).hexdigest()

	return 'A' + email + ':' + key

def is_valid_restore(account_info):
	email, key = account_info[1:].split(':')
	correct_key = '0' + hashlib.md5(email.encode('utf-8')).hexdigest()

	return key == correct_key

def simulate_receive_email():
	# Simulate receiving emails at temporary email address
	# by sending a message to all clients every ten seconds, for a minute
	for message_id in range(6):
		time.sleep(10)
		server.send_message_to_all('I{{"to_mail_orig":"receiver@example.com","to_mail":"receiver@example.com","text_source":"text","text":"Hello, World!","subject":"Testing email {}","ref":"q0d00nrvm6fclgs6gv4a1cab4vd94jqa","received":"2019-01-31T00:00:00Z","has_html":false,"from_mail":"sender@example.com","from_hdr":"\\\"Sender Name\\\" <sender@example.com>","decode_status":0,"attached":[]}}'.format(message_id))

def on_new_client(client, server):
	session_id = generate_session_id()
	server.send_message(client, session_id)

	domain = random.choice(supported_domains)
	account_info = generate_account_info(domain)
	server.send_message(client, account_info)

	server.send_message(client, 'D' + ','.join(supported_domains))

def on_receive_message(client, server, message):
	message_type = message[:1]
	message = message[1:]

	# Client requesting a new email address with domain in message
	if message_type == 'A':
		domain = message

		if domain not in supported_domains:
			server.send_message(client, 'W001Unknown domain')
			return

		account_info = generate_account_info(domain)
		server.send_message(client, account_info)
	# Client requesting a new email address (any domain)
	elif message_type == 'M':
		domain = random.choice(supported_domains)
		account_info = generate_account_info(domain)
		server.send_message(client, account_info)
	# Client restoring an address
	elif message_type == 'R':
		# Client does not send the prefix
		# so prepend it before checking validity
		account_info = 'A' + message

		if not is_valid_restore(account_info):
			server.send_message(client, 'W002Bad restore key')
			return

		server.send_message(client, 'A{}'.format(account_info))

thread = threading.Thread(target=simulate_receive_email, args=())
thread.daemon = True
thread.start()

server = websocket_server.WebsocketServer(port=9001)
server.set_fn_new_client(on_new_client)
server.set_fn_message_received(on_receive_message)
server.run_forever()